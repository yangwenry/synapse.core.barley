package org.apache.synapse;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ServiceGroupContext;
import org.apache.axis2.deployment.DeploymentEngine;
import org.apache.axis2.deployment.DeploymentLifeCycleListener;
import org.apache.axis2.deployment.util.Utils;
import org.apache.axis2.description.AxisModule;
import org.apache.axis2.description.AxisServiceGroup;
import org.apache.axis2.description.Parameter;
import org.apache.axis2.description.TransportOutDescription;
import org.apache.axis2.engine.AxisConfiguration;
import org.apache.axis2.engine.AxisConfigurator;
import org.apache.axis2.engine.DependencyManager;
import org.apache.axis2.i18n.Messages;
import org.apache.axis2.modules.Module;
import org.apache.axis2.transport.TransportSender;
import org.apache.axis2.util.SessionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

// (추가) 2019.06.25 - 직접 만듬.
public class SynapseConfigurationContextFactory {
	
	protected static final Log log = LogFactory.getLog(SynapseConfigurationContextFactory.class);

	public static ConfigurationContext createConfigurationContext(
            AxisConfigurator axisConfigurator) throws AxisFault {
        AxisConfiguration axisConfig = axisConfigurator.getAxisConfiguration();
        // call to the deployment listners
        Parameter param = axisConfig.getParameter(Constants.Configuration.DEPLOYMENT_LIFE_CYCLE_LISTENER);
        DeploymentLifeCycleListener deploymentLifeCycleListener = null;
        if (param != null){
            String className = (String) param.getValue();
            try {
                deploymentLifeCycleListener = (DeploymentLifeCycleListener) Class.forName(className).newInstance();
            } catch (InstantiationException e) {
                log.error("Can not instantiate deployment Listener " + className, e);
                throw new AxisFault("Can not instantiate deployment Listener " + className);
            } catch (IllegalAccessException e) {
                log.error("Illegal Access deployment Listener " + className, e);
                throw new AxisFault("Illegal Access deployment Listener " + className);
            } catch (ClassNotFoundException e) {
                log.error("Class not found deployment Listener " + className, e);
                throw new AxisFault("Class not found deployment Listener " + className);
            }
        }
        if (deploymentLifeCycleListener != null){
            deploymentLifeCycleListener.preDeploy(axisConfig);
        }
        ConfigurationContext configContext = new ConfigurationContext(axisConfig);

        if (axisConfigurator instanceof DeploymentEngine) {
            ((DeploymentEngine) axisConfigurator).setConfigContext(configContext);
        }
        
        //To override context path
        setContextPaths(axisConfig, configContext);
        init(configContext);
        // (주석) 여기서 모듈을 로드하지 말자. 모듈을 로드하면 예외가 발생.
        //axisConfigurator.engageGlobalModules();
        //axisConfigurator.loadServices();
        addModuleService(configContext);

        // TODO: THIS NEEDS A TEST CASE!
        initApplicationScopeServices(configContext);

        //Check whether there are any faulty services due to modules and trasports,
        //If any, let the user know.
        Utils.logFaultyServiceInfo(axisConfig);

        axisConfig.setStart(true);
        if (deploymentLifeCycleListener != null){
            deploymentLifeCycleListener.postDeploy(configContext);
        }

        // Finally initialize the cluster
        if (axisConfig.getClusteringAgent() != null) {
            configContext.initCluster();
        }
        
        return configContext;
    }
	
	private static void initApplicationScopeServices(ConfigurationContext configCtx)
            throws AxisFault {
        Iterator serviceGroups = configCtx.getAxisConfiguration().getServiceGroups();
        while (serviceGroups.hasNext()) {
            AxisServiceGroup axisServiceGroup = (AxisServiceGroup) serviceGroups.next();
            String maxScope = SessionUtils.calculateMaxScopeForServiceGroup(axisServiceGroup);
            if (Constants.SCOPE_APPLICATION.equals(maxScope)) {
                ServiceGroupContext serviceGroupContext =
                        configCtx.createServiceGroupContext(axisServiceGroup);
                configCtx.addServiceGroupContextIntoApplicationScopeTable(serviceGroupContext);
                DependencyManager.initService(serviceGroupContext);
            }
        }
    }
	
    private static void setContextPaths(AxisConfiguration axisConfig, ConfigurationContext configContext) {
    	// Checking for context path
        Parameter servicePath = axisConfig.getParameter(Constants.PARAM_SERVICE_PATH);
        if (servicePath != null) {
            String spath = ((String) servicePath.getValue()).trim();
            if (spath.length() > 0) {
                configContext.setServicePath(spath);
            }
        } else {
            configContext.setServicePath(Constants.DEFAULT_SERVICES_PATH);
        }

        Parameter contextPath = axisConfig.getParameter(Constants.PARAM_CONTEXT_ROOT);
        if (contextPath != null) {
            String cpath = ((String) contextPath.getValue()).trim();
            if (cpath.length() > 0) {
                configContext.setContextRoot(cpath);
            }
        } else {
            configContext.setContextRoot("axis2");
        }
	}
    
    private static void init(ConfigurationContext configContext) {
        initModules(configContext);
        initTransportSenders(configContext);
    }

    private static void initModules(ConfigurationContext context) {
        AxisConfiguration configuration = context.getAxisConfiguration();
        HashMap modules = configuration.getModules();
        Collection col = modules.values();
        Map faultyModule = new HashMap();

        for (Iterator iterator = col.iterator(); iterator.hasNext();) {
            AxisModule axismodule = (AxisModule) iterator.next();
            Module module = axismodule.getModule();

            if (module != null) {
                try {
                    module.init(context, axismodule);
                } catch (AxisFault axisFault) {
                    log.info(axisFault.getMessage());
                    faultyModule.put(axismodule, axisFault);
                }
            }
        }

        //Checking whether we have found any faulty services during the module initilization ,
        // if so we need to mark them as fautyModule and need to remove from the modules list
        if (faultyModule.size() > 0) {
            Iterator axisModules = faultyModule.keySet().iterator();
            while (axisModules.hasNext()) {
                AxisModule axisModule = (AxisModule) axisModules.next();
                String fileName;
                if (axisModule.getFileName() != null) {
                    fileName = axisModule.getFileName().toString();
                } else {
                    fileName = axisModule.getName();
                }
                configuration.getFaultyModules().put(fileName, faultyModule.get(axisModule).toString());
                //removing from original list
                configuration.removeModule(axisModule.getName(), axisModule.getName());
            }
        }


    }

    private static void initTransportSenders(ConfigurationContext configContext) {
        AxisConfiguration axisConf = configContext.getAxisConfiguration();

        // Initialize Transport Outs
        HashMap transportOuts = axisConf.getTransportsOut();

        Iterator values = transportOuts.values().iterator();

        while (values.hasNext()) {
            TransportOutDescription transportOut = (TransportOutDescription) values.next();
            TransportSender sender = transportOut.getSender();

            if (sender != null) {
                try {
                    sender.init(configContext, transportOut);
                } catch (AxisFault axisFault) {
                    log.info(Messages.getMessage("transportiniterror", transportOut.getName()));
                }
            }
        }
    }
    
    private static void addModuleService(ConfigurationContext configCtx) throws AxisFault {
        AxisConfiguration axisConfig = configCtx.getAxisConfiguration();
        HashMap modules = axisConfig.getModules();
        if (modules != null && modules.size() > 0) {
            Iterator mpduleItr = modules.values().iterator();
            while (mpduleItr.hasNext()) {
                AxisModule axisModule = (AxisModule) mpduleItr.next();
                Utils.deployModuleServices(axisModule, configCtx);
            }
        }
    }
}
